# 2D Fractals - Benoit Mandelbrot & Gaston Julia Sets

> A [**_3rd year's project_**](http://www.unl.pt/guia/2018/fct/UNLGI_getCurso?curso=935) of the [**_BSc. degree of Computer Sciences and Engineering_**](https://www.fct.unl.pt/en/education/course/integrated-master-computer-science/) made in [**_FCT NOVA (Faculty of Sciences and Technology of New University of Lisbon)_**](https://www.fct.unl.pt/), in the subject of [**_Computer Graphics and Interfaces_**](http://www.unl.pt/guia/2018/fct/UNLGI_getUC?uc=8150). This project was built using [**_HTML_**](https://www.w3schools.com/html/), [**_CSS_**](https://www.w3schools.com/css/), [**_JavaScript_**](https://www.w3schools.com/js/) and [**_WebGL_**](https://get.webgl.org/). The application allows to see [**_Fractals' structures_**](https://en.wikipedia.org/wiki/Fractal) with _multiple scales_ and, using some of the both [**_Benoit Mandelbrot_**](https://en.wikipedia.org/wiki/Benoit_Mandelbrot) and [**_Gaston Julia_**](https://en.wikipedia.org/wiki/Gaston_Julia)'s sets, in the _canvas_ of the _Web browser_!

> You can view an _online demo_, [clicking here](https://rubenandrebarreiro.github.io/projects/webgl/2d-fractals-mandelbrot-julia/2d-fractals-mandelbrot-julia.html)!


## Screenshots

![https://raw.githubusercontent.com/rubenandrebarreiro/2d-fractals-mandelbrot-julia/master/imgs/screenshot-1.jpg](https://raw.githubusercontent.com/rubenandrebarreiro/2d-fractals-mandelbrot-julia/master/imgs/screenshot-1.jpg)
######  2D Fractals (Benoit Mandelbrot & Gaston Julia Sets) - Screenshot #1

***

![https://raw.githubusercontent.com/rubenandrebarreiro/2d-fractals-mandelbrot-julia/master/imgs/screenshot-2.jpg](https://raw.githubusercontent.com/rubenandrebarreiro/2d-fractals-mandelbrot-julia/master/imgs/screenshot-2.jpg)
######  2D Fractals (Benoit Mandelbrot & Gaston Julia Sets) - Screenshot #2

***


## Getting Started

### Prerequisites
To install and run this application, you will only need:
> A **_Web browser_**, like:
* [**_Google Chrome_**](https://www.google.com/chrome/), [**_Mozilla Firefox_**](https://www.mozilla.org/), [**_Internet Explorer_**](https://www.microsoft.com/download/internet-explorer.aspx), [**_Opera_**](https://www.opera.com/) or [**_Safari_**](https://www.apple.com/safari/).
> The [**_Git_**](https://git-scm.com/) feature and/or a [**_third-party Git Client based GUI_**](https://git-scm.com/downloads/guis/), like:
* [**_GitHub Desktop_**](https://desktop.github.com/), [**_GitKraken_**](https://www.gitkraken.com/), [**_SourceTree_**](https://www.sourcetreeapp.com/) or [**_TortoiseGit_**](https://tortoisegit.org/).

### Installation
To install this application, you will only need to _download_ or _clone_ this repository and run the application locally:

> You can do it downloading the [**_.zip file_**](https://github.com/rubenandrebarreiro/2d-fractals-mandelbrot-julia/archive/master.zip) in download section of this repository.

> Or instead, by cloning this repository by a [**_Git Client based GUI_**](https://git-scm.com/downloads/guis), using [**_HTTPS_**](https://en.wikipedia.org/wiki/HTTPS) or [**_SSH_**](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol), by one of the following link:
* [**_HTTPS_**](https://en.wikipedia.org/wiki/HTTPS):
```
https://github.com/rubenandrebarreiro/2d-fractals-mandelbrot-julia.git
```
* [**_SSH_**](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol):
```
git@github.com:rubenandrebarreiro/2d-fractals-mandelbrot-julia.git
```

> Or even, by running one of the following commands in a **_Git Bash Console_**:
* [**_HTTPS_**](https://en.wikipedia.org/wiki/HTTPS):
```
git clone https://github.com/rubenandrebarreiro/2d-fractals-mandelbrot-julia.git
```
* [**_SSH_**](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol):
```
git clone git@github.com:rubenandrebarreiro/2d-fractals-mandelbrot-julia.git
```

## After the instalation
You can run the application, by open the following file presented in the _root_ folder of this repository:
```
2d-fractals-mandelbrot-julia.html
```

Instructions (also available in the _canvas_ of the application):

1) Use **_'Q'_** and **_'A'_** keys dive/emerge

2) Use **_click/drag/release_** in the **_canvas_** to move center point

3) Choose the current **_Fractal's Set_** in use (**_Benoit Mandelbrot_** or **_Gaston Julia_**)

4) Choose also the current **_Factor_** to the **_Scale_** of the current **_Fractal_** in use

#### Notes (IMPORTANT)
1) It's **_mandatory_** to have the folder called **_common_** in the local repository, because it's where are the necessary **_WebGL utilities_**, **_geometric/solid figures_** and **_other libraries_**

2) It's also **_mandatory_** to have the respective application's **_JavaScript_** file in the _root_ folder of this repository
```
2d-fractals-mandelbrot-julia.js
```

## Built with
* [**_HTML_**](https://www.w3schools.com/html/)
* [**_CSS_**](https://www.w3schools.com/css/)
* [**_JavaScript_**](https://www.w3schools.com/js/)
* [**_WebGL_**](https://get.webgl.org/)
* [**_Atom_**](https://atom.io/)
* [**_Brackets_**](http://brackets.io/)

## Contributors
> Daniel Filipe Pimenta

> [Rúben André Barreiro](https://github.com/rubenandrebarreiro/)

## Contacts
### Daniel Filipe Pimenta
* [d.pimenta@campus.fct.unl.pt](mailto:d.pimenta@campus.fct.unl.pt)

### Rúben André Barreiro
#### GitHub's Portfolio/Personal Blog
* [https://rubenandrebarreiro.github.io/](https://rubenandrebarreiro.github.io/)

#### Repositories' Services
* [https://github.com/rubenandrebarreiro/](https://github.com/rubenandrebarreiro/)
* [https://gitlab.com/rubenandrebarreiro/](https://gitlab.com/rubenandrebarreiro/)
* [https://bitbucket.org/rubenandrebarreiro/](https://bitbucket.org/rubenandrebarreiro/)

#### E-mails
* [ruben.barreiro.92@gmail.com](mailto:ruben.barreiro.92@gmail.com)
* [r.barreiro@campus.fct.unl.pt](mailto:r.barreiro@campus.fct.unl.pt)
* [up201808917@fe.up.pt](mailto:up201808917@fe.up.pt)
* [up201808917@g.uporto.pt](mailto:up201808917@g.uporto.pt)
